<!DOCTYPE html>
<html>
  <head>
    <title>Piotr Wypych CV</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Dosis&display=swap"
      rel="stylesheet"
    />
    <script>
      // Picture element HTML5 shiv
      document.createElement("picture");
    </script>
    <script
      src="https://cdn.rawgit.com/scottjehl/picturefill/3.0.2/dist/picturefill.js"
      async
    ></script>
  </head>
  <body>
    <!-- header.php -->
    <?php require 'partials/header.php'; ?>
    <!-- info.php -->
    <?php require 'partials/info.php'; ?>
    <!-- about.php -->
    <?php require 'partials/about.php'; ?>
    <!-- experience.php -->
    <?php require 'partials/experience.php'; ?>
    <!-- education.php -->
    <?php require 'partials/education.php'; ?>
    <!-- skills.php -->
    <?php require 'partials/skills.php'; ?>
    <!-- portfolio.php -->
    <?php require 'partials/portfolio.php'; ?>
    <!-- contact.php -->
    <?php require 'partials/contact.php'; ?>

    <footer>© 2019 Piotr Wypych</footer>
  </body>

  <script src="public/lib/jquery-3.4.1.js"></script>
  <script src="public/js/menu.js"></script>
  <script src="public/js/animations.js"></script>
</html>
