let sectionVisibilityLine;
console.log("sectionVisibilityLine:", sectionVisibilityLine);

$(() => {
  console.log("naimations.js ready!");

  $("#info")
    .hide()
    .fadeIn(1000);

  $(".personal-info")
    .hide()
    .slideDown(2000);

  $(document).scroll(() => {
    var y = $(this).scrollTop();
    sectionVisibilityLine = y + $(window).height() / 1.2;

    animateWidthByClassName(".about-description");
    animateWidthByClassName(".experience-entry");
    animateWidthByClassName(".education-entry");
    animateWidthByClassName(".project");

    // Skills section
    if (sectionVisibilityLine > $(".skills-wrapper").offset().top) {
      skillsAnimation();
    }
  });
});

// ---------- Functions definitions ----------
function animateWidthByClassName(className) {
  $(className).each((key, val) => {
    const isHideClass = $(val).hasClass("hide");
    const offsetTop = $(val).offset().top;
    if (sectionVisibilityLine > offsetTop && isHideClass) {
      $(val).animate(
        {
          opacity: 1,
          width: "show"
        },
        1000,
        () => {
          $(val).removeClass("hide");
        }
      );
    }
  });
}

function skillsAnimation() {
  $(".skill").each((key, val) => {
    $(val).animate(
      {
        opacity: 1
      },
      2000,
      () => {}
    );
    $(val.firstChild).animate(
      {
        width: "100%"
      },
      2000 + key * 100
    );
  });
}
