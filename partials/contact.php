<h3 class="section-header grey-bg" id="opt-5">Contact me</h3>
<section id="contact">
  <div class="contact-wrapper">
    <div class="contact-form">
      <form
        action="https://formspree.io/ludzik3217@gmail.com"
        method="POST"
      >
        <input
          class="input-name"
          type="text"
          name="name"
          placeholder="Name"
        />
        <input
          class="input-email"
          type="text"
          name="email"
          placeholder="Email"
        />
        <input
          class="input-subject"
          type="text"
          name="subject"
          placeholder="Subject"
        />
        <textarea
          class="input-message"
          name="text"
          rows="6"
          placeholder="Message"
        ></textarea>
        <button class="send-message">Submit</button>
      </form>
    </div>

    <div class="contact-info">
      <div class="info-wrapper">
        <h3 class="headline-name">Piotr Wypych</h3>
        <h3 class="proffesion">Web developer</h3>
      </div>
      <div class="info-wrapper">
        <h3 class="info-headline">Phone</h3>
        <p class="info-main">123-456-789</p>
      </div>
      <div class="info-wrapper">
        <h3 class="info-headline">Email</h3>
        <p class="info-main">example@gmail.com</p>
      </div>
      <div class="contact-media-wrapper">
        <ul class="contact-media">
          <li class="media-icon">
            <a href="https://gitlab.com/pwypych" target="_blank"
              ><img
                src="public/images/gitlab-icon.png"
                title="GitLab"
                alt="GitLab icon"
            /></a>
          </li>
          <li class="media-icon">
            <a
              href="https://www.linkedin.com/in/piotrwypych/"
              target="_blank"
              ><img
                src="public/images/linkenin-icon.png"
                title="LinkedIn"
                alt="LinkedIn icon"
            /></a>
          </li>
          <li class="media-icon">
            <a href="https://www.facebook.com/josef.sopp" target="_blank"
              ><img
                src="public/images/facebook icon.png"
                title="Facebook"
                alt="Facebook icon"
            /></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
