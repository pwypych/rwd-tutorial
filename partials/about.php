<section id="about">
  <div class="social-media">
    <ul class="social-media-list">
      <li class="social-media-item">
        <a href="https://gitlab.com/pwypych" target="_blank"
          ><img
            src="public/images/gitlab-icon-white.png"
            title="GitLab"
            alt="GitLab icon"
        /></a>
      </li>
      <li class="social-media-item">
        <a href="https://www.linkedin.com/in/piotrwypych/" target="_blank"
          ><img
            src="public/images/linkenin-icon-white.png"
            title="LinkedIn"
            alt="LinkedIn icon"
        /></a>
      </li>
      <li class="social-media-item">
        <a href="https://www.facebook.com/josef.sopp" target="_blank"
          ><img
            src="public/images/facebook-icon-white.png"
            title="Facebook"
            alt="Facebook icon"
        /></a>
      </li>
    </ul>
  </div>
  <div class="about-section-wrapper">
    <h3 class="about-header">Hello! I'm Piotr</h3>
    <div class="about-description hide">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
      minim veniam, quis nostrud exercitation ullamco laboris nisi ut
      aliquip ex ea commodo consequat. Duis aute irure dolor in
    </div>
  </div>
</section>
