<header>
  <div class="name">
    <h1>Piotr Wypych</h1>
    <a class="burger-nav"></a>
  </div>
  <nav class="nav-wrapper">
    <ul class="nav-list">
      <li class="nav-item"><a href="#opt-1">experience</a></li>
      <li class="nav-item"><a href="#opt-2">education</a></li>
      <li class="nav-item"><a href="#opt-3">skills</a></li>
      <li class="nav-item"><a href="#opt-4">projects</a></li>
      <li class="nav-item"><a href="#opt-5">contact</a></li>
    </ul>
  </nav>
</header>
