<section id="info" class="parallax">
  <div class="info-section-wrapper">
    <div class="profile-photo">
      <picture>
        <source
          srcset="public/images/profile-photo-m.jpg"
          media="(max-width: 1500px)"
        />
        <img srcset="public/images/profile-photo.jpg" />
      </picture>
    </div>

    <div class="personal-info-wrapper">
      <div class="personal-info">
        <div class="info-wrapper">
          <h3 class="headline-name">Piotr Wypych</h3>
          <h3 class="proffesion">Web developer</h3>
        </div>
        <div class="info-wrapper">
          <h3 class="info-headline">Phone:</h3>
          <p class="info-main">123 456 789</p>
        </div>
        <div class="info-wrapper">
          <h3 class="info-headline">Email:</h3>
          <p class="info-main">example@gmail.com</p>
        </div>
        <div class="info-wrapper">
          <h3 class="info-headline">Date of birth:</h3>
          <p class="info-main">29.11.1993</p>
        </div>
        <div class="info-wrapper">
          <h3 class="info-headline">Address:</h3>
          <p class="info-main">Miechów, Małopolska, Poland</p>
        </div>
      </div>
    </div>
  </div>
</section>
