<h3 class="section-header" id="opt-1">Experience</h3>
<section id="experience">
  <div class="experience-entry hide">
    <div class="date">
      <h5 class="date-headline">2020-2023</h5>
      <p class="job">Developer</p>
    </div>
    <div class="indicator"></div>
    <div class="company">
      <h5 class="company-headline">Company</h5>
      <p class="company-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
    </div>
  </div>

  <div class="experience-entry hide">
    <div class="date">
      <h5 class="date-headline">2020-2023</h5>
      <p class="job">Developer</p>
    </div>
    <div class="indicator"></div>
    <div class="company">
      <h5 class="company-headline">Company</h5>
      <p class="company-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
    </div>
  </div>
</section>
