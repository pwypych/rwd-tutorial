<h3 class="section-header" id="opt-4">Portfolio</h3>
<section id="portfolio">
  <a href="https://axe.cogs.ovh/panel" target="_blank"
    ><div class="project hide">
      <h5 class="project-title">Champions of Guilds and Strongholds</h5>
      <div class="project-image">
        <img src="public/images/cogs.PNG" alt="project photo" />
      </div>
      <div class="project-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </div>
      <div class="technology-stack">
        Stack:
        <ul class="stack-list">
          <li class="stack-item">Node.js</li>
          <li class="stack-item">MongoDB</li>
          <li class="stack-item">Express.js</li>
          <li class="stack-item">HTML5</li>
          <li class="stack-item">pixi.js</li>
          <li class="stack-item">jQuery</li>
        </ul>
      </div>
    </div></a
  >

  <a href="https://gitlab.com/pwypych/rwd-tutorial" target="_blank"
    ><div class="project hide">
      <h5 class="project-title">Resume page</h5>
      <div class="project-image">
        <img src="public/images/resumePage.PNG" alt="project photo" />
      </div>
      <div class="project-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </div>
      <div class="technology-stack">
        Stack:
        <ul class="stack-list">
          <li class="stack-item">HTML5</li>
          <li class="stack-item">CSS3</li>
          <li class="stack-item">jQuery</li>
          <li class="stack-item">RWD</li>
        </ul>
      </div>
    </div></a
  >

  <a href="https://gitlab.com/pwypych/flappy_dragon" target="_blank"
    ><div class="project hide">
      <h5 class="project-title">Flappy Dragon</h5>
      <div class="project-image">
        <img src="https://via.placeholder.com/150C/O" alt="project photo" />
      </div>
      <div class="project-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </div>
      <div class="technology-stack">
        Stack:
        <ul class="stack-list">
          <li class="stack-item">Lua</li>
          <li class="stack-item">Love</li>
          <li class="stack-item">HTML5</li>
        </ul>
      </div>
    </div></a
  >
</section>
