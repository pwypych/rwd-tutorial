<h3 class="section-header" id="opt-3">Skills</h3>
<section id="skills" class="parallax">
  <div class="skills-wrapper">
    <div class="skill hide"><span>JavaScript - Advanced</span></div>
    <div class="skill hide"><span>MongoDB - Advanced</span></div>
    <div class="skill hide"><span>Node.js - Advanced</span></div>
    <div class="skill hide"><span>Express.js - Advanced</span></div>
    <div class="skill hide"><span>HTML 5 - Advanced</span></div>
    <div class="skill hide"><span>CSS 3 - Advanced</span></div>
    <div class="skill hide"><span>PHP - Advanced</span></div>
    <div class="skill hide"><span>WordPress - Advanced</span></div>
    <div class="skill hide"><span>Pixi.js - Advanced</span></div>
    <div class="skill hide"><span>RWD - Advanced</span></div>
  </div>
</section>
