<h3 class="section-header" id="opt-2">Education</h3>
<section id="education">
  <div class="education-entry hide">
    <div class="date">
      <h5 class="date-headline">2015-2018</h5>
      <p class="degree">Master's Degree</p>
    </div>
    <div class="indicator"></div>
    <div class="school">
      <h5 class="school-headline">School</h5>
      <p class="school-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
    </div>
  </div>

  <div class="education-entry hide">
    <div class="date">
      <h5 class="date-headline">2015-2018</h5>
      <p class="degree">Master's Degree</p>
    </div>
    <div class="indicator"></div>
    <div class="school">
      <h5 class="school-headline">School</h5>
      <p class="school-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
    </div>
  </div>
</section>
